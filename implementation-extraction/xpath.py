import json
from lxml import etree
from io import StringIO


def read_file(path):
    with open(path, errors="ignore") as f:
        return f.read().replace('\n', '')


def read_file_utf(path):
    with open(path, encoding="utf-8", errors="ignore") as f:
        return f.read().replace('\n', ' ')


def extract_overstock_xpath(path):
    """ Extract Title, ListPrice, Price, Content, Saving and SavingPercent from Overstock page """

    html = read_file(path)

    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(html), parser)

    titles = tree.xpath('//td[@valign="top"]/a/b')
    list_prices = tree.xpath('//td/b[text()="List Price:"]/ancestor::tr[1]/td/s')
    prices = tree.xpath('//td/b[text()="Price:"]/ancestor::tr[1]/td/span[@class="bigred"]/b')
    saving_percent = tree.xpath('//td/b[text()="You Save:"]/ancestor::tr[1]/td/span[@class="littleorange"]')
    contents = tree.xpath('//td/span[@class="normal"]')

    result = []
    for i in range(len(titles)):
        title = titles[i].text
        list_price = list_prices[i].text
        price = prices[i].text
        save_percent = saving_percent[i].text
        save = save_percent.split(' ')[0]
        percent = save_percent.split(' ')[1].replace('(', '').replace(')', '')
        content = contents[i].text
        result.append({
            "Title": title,
            "ListPrice": list_price,
            "Price": price,
            "Saving": save,
            "SavingPercent": percent,
            "Content": content
        })

    result = json.dumps(result, indent=4)

    return result


def extract_rtvslo_xpath(path):
    """ Extract Title, Author, PublishedTime, SubTitle, Lead, Content from RtvSlo """

    html = read_file_utf(path)

    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(html), parser)

    title = tree.xpath('//header[@class="article-header"]//h1')
    subtitle = tree.xpath('//header[@class="article-header"]//div[@class="subtitle"]')
    author = tree.xpath('//div[@class="author"]//div[@class="author-name"]')
    published_time = tree.xpath('//div[@class="article-meta"]//div[@class="publish-meta"]')
    lead = tree.xpath('//header[@class="article-header"]//p[@class="lead"]')
    contents = tree.xpath('//div[contains(@class, "article-body")]//*[self::figcaption or self::p]/text()')

    content = ""
    for part in contents:
        content += part.strip() + " "

    result = {
        "Author": author[0].text,
        "PublishedTime": published_time[0].text.replace('\t', ''),
        "Title": title[0].text,
        "SubTitle": subtitle[0].text,
        "Lead": lead[0].text,
        "Content": content
    }

    result = json.dumps(result, indent=4, ensure_ascii=False)

    return result


def extract_imdb_xpath(path):
    """ Extract Title, Rating, Year, Length, Release, Content from IMDB page """

    html = read_file_utf(path)

    parser = etree.HTMLParser()
    tree = etree.parse(StringIO(html), parser)

    title = tree.xpath('//div[@class="title_wrapper"]/h1/text()')
    rating = tree.xpath('//div[@class="ratings_wrapper"]/div[@class="imdbRating"]/div/strong/span/text()')
    year = tree.xpath('//div[@class="title_wrapper"]/h1/span[@id="titleYear"]/a/text()')
    length = tree.xpath('//div[@class="title_wrapper"]/div/time/text()[normalize-space()]')
    release = tree.xpath('//div[@class="title_wrapper"]/div/a[4]/text()')
    content = tree.xpath('//div[@class="summary_text"]/text()[normalize-space()]')

    result = {
        "Rating": rating[0],
        "Title": title[0].replace('\xa0', ''),
        "Year": year[0],
        "Length": length[0].strip(),
        "Release": release[0].strip(),
        "Content": content[0].strip()
    }

    result = json.dumps(result, indent=4)

    return result


if __name__ == "__main__":
    extract_overstock_xpath("../input-extraction/overstock.com/jewelry01.html")
    # extract_rtvslo_xpath("../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html")
    # extract_imdb_xpath("../input-extraction/imdb.com/Najvecji sovmen (2017) - IMDb.html")