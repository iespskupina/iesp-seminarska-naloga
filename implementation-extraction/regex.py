import re
import json

def read_file(name):
    with open(name) as file:
        return file.read().replace('\n', '')

def read_file_utf(name):
    with open(name, encoding="utf-8") as file:
        return file.read().replace('\n', ' ')

def extract_overstock(page):
    text = read_file(page)

    regnumber = r"\$((?:[0-9],)?[0-9]+\.[0-9]{2})"
    reg = r"<tr bgcolor=\"(?:#ffffff|#dddddd)\">\s<td valign=\"top\" align=\"center\">.+?<a.+?><b>(.+?)<\/b><\/a><br>.+?List Price:.+?<s>" + regnumber + r"<\/s>.*?Price:.*?" + regnumber + r".*?You Save:.*?" + regnumber + r" \(([0-9]+%)\).*?<td valign=\"top\"><span class=\"normal\">(.*?)<br>"

    # print(reg)

    matches = re.finditer(reg, text)

    # print(len(list(matches)))
    dataitems = []
    for match in matches:
        title = match.group(1)
        listprice = match.group(2)
        price = match.group(3)
        saving = match.group(4)
        savingpercent = match.group(5)
        content = match.group(6)
        dataitems.append({
            "Title": title,
            "ListPrice": listprice,
            "Price": price,
            "Saving": saving,
            "SavingPercent": savingpercent,
            "Content": content
        })
    return dataitems


def extract_rtv(page):
    text = read_file_utf(page)
    # print(text)

    reg = r"<h1>(.*?)<\/h1>\s*<div class=\"subtitle\">(.*?)<\/div>.*?<div class=\"author-timestamp\">\s*<strong>(.*?)</strong>\| (.*?)\s*<\/div>.*?<p class=\"lead\">(.*?)<\/p>.*?<article class=\"article\">(.*?)<\/article>"
    # print(reg)

    match = re.compile(reg).search(text)
    title = match.group(1)
    subtitle = match.group(2)
    author = match.group(3)
    timestamp = match.group(4)
    lead = match.group(5)

    content = match.group(6)
    regscript = re.compile(r"<script.*?</script>")
    content = regscript.sub('', content)
    reghtml = re.compile(r'<[^>]+>')
    content = reghtml.sub(' ', content)#.replace("MMC RTV SLO", "")
    regtab = re.compile(r'\s+')
    content = regtab.sub(' ', content)

    dataitem = {
        "Author": author,
        "PublishedTime": timestamp,
        "Title": title,
        "Subtitle": subtitle,
        "Lead": lead,
        "Content": content
    }
    # print(json.dumps(dataitem, indent=4, ensure_ascii=False))
    return dataitem

def extract_imdb(page):
    text = read_file(page)

    reg = r"<span itemprop=\"ratingValue\">(.*?)<\/span>.*?<h1 class=\"\">\s*(.*?)<span id=\"titleYear\">\s*\(<a.*?>(\d{4})<\/a>.*?<time datetime=.*?>\s*(.*?)\s*<\/time>\s*<span class=\"ghost\">\|</span>.*?<span class=\"ghost\">\|</span>\s*<a .*?>(.*?)<\/a>.*?<div class=\"summary_text\">\s*(.*?)\s*<\/div>"
    # print(reg)

    match = re.compile(reg).search(text)
    rating = match.group(1)
    title = match.group(2).replace("&nbsp;", "")
    year = match.group(3)
    length = match.group(4)
    release = match.group(5)
    content = match.group(6)

    dataitem = {
        "Rating": rating,
        "Title": title,
        "Year": year,
        "Length": length,
        "Release": release,
        "Content": content
    }
    # print(json.dumps(dataitem, indent=4))
    return dataitem


if __name__ == "__main__":
    print(extract_overstock("../input-extraction/overstock.com/jewelry01.html"))
    # extract_rtv("../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html")
    # print(extract_rtv("../input-extraction/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljs╠îe v razredu - RTVSLO.si.html"))
    # extract_imdb("../input-extraction/imdb.com/Najvecji sovmen (2017) - IMDb.html")
    # extract_imdb("../input-extraction/imdb.com/PK (2014) - IMDb.html")