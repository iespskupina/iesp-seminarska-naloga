def align(list1, list2, compare):
    i = 0
    while i < len(list1) and i < len(list2):
        dodajanje1 = koliko_dodati(list1[i:], list2[i:], compare)
        dodajanje2 = koliko_dodati(list2[i:], list1[i:], compare)
        if dodajanje1 == -1 and dodajanje2 == -1:
            list1.insert(i, None)
            i += 1
            list2.insert(i, None)
            i += 1
        else:
            if dodajanje2 == -1 or dodajanje1 < dodajanje2:
                for j in range(dodajanje1):
                    list1.insert(i, None)
                    i += 1
            if dodajanje1 == -1 or dodajanje2 <= dodajanje1:
                for j in range(dodajanje2):
                    list2.insert(i, None)
                    i += 1
        if dodajanje1 == 0 and dodajanje2 == 0:
            i += 1
        pass
    while len(list1) < len(list2):
        list1.append(None)
    while len(list2) < len(list1):
        list2.append(None)

    return (list1, list2)


def koliko_dodati(seznam_za_dodajanje, seznam_za_primerjavo, primerjava):
    i = 0
    while i < 3 and i < len(seznam_za_primerjavo) and not primerjava(seznam_za_dodajanje[0], seznam_za_primerjavo[i]):
        i += 1
    if i >= len(seznam_za_primerjavo) or i >= 3:
        return -1
    return i


def primerjaj(a, b):
    return a == b


zaznane_ponovitve = 3


def element(seznam1, seznam2, i):
    if seznam1[i] is None:
        return seznam2[i]
    return seznam1[i]


def zaznaj_ponavlajnja(seznam1, seznam2, primerjaj):
    izhod = []
    ponovitve = []
    i = 0
    while i < len(seznam1):
        se_ujema = False
        for j in range(1, min(i, len(seznam1) - i, zaznane_ponovitve)+1):
            # preveri j elementov
            se_ujema = True
            for a in range(j):
                if not primerjaj(element(seznam1, seznam2, i + a), element(seznam1, seznam2, i - j + a)) and not primerjaj(element(seznam2,seznam1, i + a), element(seznam2, seznam1, i - j + a)):
                    se_ujema = False

            if se_ujema:
                vsota = 0
                while vsota < j:
                    vsota+=abs(ponovitve[-1])
                    ponovitve = ponovitve[:-1]
                ponovitve.append(vsota)
                i += j
                break
        if not se_ujema:
            izhod.append(i)
            ponovitve.append(-1)
            i += 1
    return (ponovitve, izhod)


if __name__ == '__main__':
    print(zaznaj_ponavlajnja([1, 2, 3, 2, 3, 4, 5], [1, 2, 3, 2, 3, 4, 5], primerjaj))
    # print(align([1, 2, 3, 4, 5], [1, 2, 4, 5], primerjaj))
    # print(align([1, 1, 1, 1, 1], [1, 1, 1, 1], primerjaj))
    # print(align([2, 1, 1, 1, 1], [1, 1, 1, 1], primerjaj))
    # print(align([1, 2, 1, 1, 1, 1], [1, 1, 1, 1, 2, 1], primerjaj))
    # print(align([1, 2, 1, 1, 1, 1], [1, 1, 2, 1, 2, 1], primerjaj))
    # print(align([1, 2, 3, 1, 1, 1], [1, 1, 1, 1, 1, 1], primerjaj))
