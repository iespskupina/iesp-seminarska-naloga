from lxml import html, etree
from io import StringIO
import list_alignment



def read_file(path):
    with open(path, encoding="utf-8", errors="ignore") as f:
        return f.read().replace('\n', '')


def road_runner(path1: str, path2: str):
    html1 = read_file(path1)
    html2 = read_file(path2)

    parser = etree.HTMLParser()
    tree1 = etree.parse(StringIO(html1), parser)
    tree2 = etree.parse(StringIO(html2), parser)
    return (sestavi_izraz(tree1.getroot().find("body"), tree2.getroot().find("body"))[0])


def zapisi_element(element):
    if not isinstance(element.tag, str):
        # Komentar
        return ""
    # zacetna znacka
    izraz = "<" + element.tag
    # pisanje vseh atributov
    for key in element.attrib:
        izraz += " " + key + "=\""
        izraz += skrajsajBesedilo(element.attrib[key]) + "\""
    izraz += ">"

    # vsebina
    if element.text is not None:
        izraz += element.text.strip()
    otroci = odstrani_komentarje(element.getchildren())
    prejsnje_besedilo = ""
    ze_zapisano = True
    for otrok in otroci:
        bes = zapisi_element(otrok)
        if bes == prejsnje_besedilo:
            if not ze_zapisano:
                ze_zapisano = True
                izraz += "(" + prejsnje_besedilo + ")*"
        else:
            if not ze_zapisano:
                izraz += prejsnje_besedilo
            prejsnje_besedilo = bes
            ze_zapisano = False

    if not ze_zapisano:
        izraz += prejsnje_besedilo
    # koncna znacka
    izraz += "</" + element.tag + ">"
    return izraz


def sestavi_izraz(element1, element2):
    imavrednost = False
    if not isinstance(element1.tag, str) or not isinstance(element2.tag, str):
        # Komentar
        return ""
    # print(element1.tag, element2.tag)
    if element1.tag == element2.tag:
        atributi1 = element1.attrib
        atributi2 = element2.attrib

        izraz = "<" + element1.tag
        # pisanje vseh atributov
        for key in atributi1:
            if key in atributi2:
                izraz += " " + key + "=\""
                if atributi1[key] == atributi2[key]:
                    izraz += skrajsajBesedilo(atributi1[key]) + "\""
                else:
                    izraz += ".*?\""
            else:
                izraz += " (" + key + "=\"" + skrajsajBesedilo(atributi1[key]) + "\")?"

        for key in atributi2:
            if key not in atributi1:
                izraz += " (" + key + "=\"" + skrajsajBesedilo(atributi2[key]) + "\")?"
        izraz += ">"

        if element1.text is not None and element2.text is not None:
            if element1.text.strip() == element2.text.strip():
                izraz += element1.text.strip()
            else:
                izraz += "#vrednost"
                imavrednost = True
        otroci1 = odstrani_komentarje(element1.getchildren())
        otroci2 = odstrani_komentarje(element2.getchildren())
        (otroci1, otroci2) = list_alignment.align(otroci1, otroci2, primerjaj_znacki)

        (ponovitve, odstranjena_ponavaljanja) = list_alignment.zaznaj_ponavlajnja(otroci1,otroci2, primerjaj_drevesi)
        # print(odstranjena_ponavaljanja)
        i = 0
        while i<len(odstranjena_ponavaljanja):
            p = ponovitve.pop()
            if p >= 0:
                izraz+="("
            for a in range(abs(p)):
                if otroci1[odstranjena_ponavaljanja[i]] is None:
                    # izraz += "(" +zapisi_element(otroci2[odstranjena_ponavaljanja[i]])+")?"
                    izraz+=".*"
                    pass
                elif otroci2[odstranjena_ponavaljanja[i]] is None:
                    # izraz += "(" +zapisi_element(otroci1[odstranjena_ponavaljanja[i]])+")?"
                    izraz+=".*"
                    pass
                elif primerjaj_drevesi(otroci1[odstranjena_ponavaljanja[i]], otroci2[odstranjena_ponavaljanja[i]]):
                    (delizraza, vsebujevrednost) = sestavi_izraz(otroci1[odstranjena_ponavaljanja[i]], otroci2[odstranjena_ponavaljanja[i]])
                    if vsebujevrednost:
                        imavrednost = True
                        izraz += delizraza
                    else:
                        izraz += "<"+otroci1[odstranjena_ponavaljanja[i]].tag+">"+".*?"+"</"+otroci1[odstranjena_ponavaljanja[i]].tag+">"
                else:
                    (delizraza, vsebujevrednost) = sestavi_izraz(otroci1[odstranjena_ponavaljanja[i]], otroci2[odstranjena_ponavaljanja[i]])
                    if vsebujevrednost:
                        imavrednost = True
                        izraz += delizraza
                    else:
                        izraz += "<"+otroci1[odstranjena_ponavaljanja[i]].tag+">"+".*?"+"</"+otroci1[odstranjena_ponavaljanja[i]].tag+">"
                i+=1

            if p >= 0:
                izraz+=")*"

        izraz += "</" + element1.tag + ">"
        # Besedilo na koncu
        if element1.tail is not None and element2.tail is not None:
            if element1.tail.strip() == element2.tail.strip():
                izraz += element1.tail.strip()
            else:
                izraz += "#vrednost"
                imavrednost = True
        return (izraz, imavrednost)

    else:
        return ("(" + zapisi_element(element1) + ")?(" + zapisi_element(element2) + ")?", imavrednost)



def odstrani_komentarje(seznam):
    odg = []
    for i in range(len(seznam)):
        if isinstance(seznam[i].tag, str) and seznam[i].tag != "script" and seznam[i].tag != "style" and seznam[
            i].tag != "iframe":
            odg.append(seznam[i])
    return odg

def primerjaj_znacki(element1, element2):
    if (element1.tag != element2.tag):
        return False
    if ("id" in element1.attrib) and ("id" in element2.attrib):
        if (len(element1.attrib["id"]) != len(element2.attrib["id"]) and element1.attrib["id"] != element2.attrib[
            "id"]):
            return False
    if ("id" in element1.attrib) != ("id" in element2.attrib):
        return False
    return True


def primerjaj_drevesi(drevo1, drevo2):
    if drevo1.tag == drevo2.tag:
        otroci1 = drevo1.getchildren()
        otroci2 = drevo2.getchildren()
        if len(otroci1) == len(otroci2):
            vsi_enaki = True
            for i in range(len(otroci1)):
                vsi_enaki = vsi_enaki and primerjaj_drevesi(otroci1[i], otroci2[i])
            return vsi_enaki
        else:
            return False
    else:
        return False


def skrajsajBesedilo(besedilo):
    return besedilo
    # if len(besedilo) > 40:
    #     return ".*?"
    # else:
    #     return besedilo


if __name__ == '__main__':
    # road_runner("../input-extraction/test/test1.html",
    #             "../input-extraction/test/test2.html")
    print(road_runner("../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html",
                "../input-extraction/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljs╠îe v razredu - RTVSLO.si.html"))
    # road_runner("../input-extraction/test/rtv1-glava.html",
    #             "../input-extraction/test/rtv2-glava.html")
    # print(road_runner("../input-extraction/overstock.com/jewelry01.html",
    #             "../input-extraction/overstock.com/jewelry02.html"))
    # road_runner("../input-extraction/test/jew1.html",
    #             "../input-extraction/test/jew2.html")
    # print(road_runner("../input-extraction/imdb.com/Najvecji sovmen (2017) - IMDb.html",
    #             "../input-extraction/imdb.com/PK (2014) - IMDb.html"))
    pass
