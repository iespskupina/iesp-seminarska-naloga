import json
import sys
from regex import extract_overstock, extract_rtv, extract_imdb
from xpath import extract_overstock_xpath, extract_rtvslo_xpath, extract_imdb_xpath
from road_runner import road_runner


if sys.argv[1] == "A":
    print(json.dumps(extract_overstock("../input-extraction/overstock.com/jewelry01.html"), indent=4))
    print(json.dumps(extract_overstock("../input-extraction/overstock.com/jewelry02.html"), indent=4))
    print(json.dumps(extract_rtv("../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html"), indent=4, ensure_ascii=False))
    print(json.dumps(extract_rtv("../input-extraction/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljs╠îe v razredu - RTVSLO.si.html"), indent=4, ensure_ascii=False))
    print(json.dumps(extract_imdb("../input-extraction/imdb.com/Najvecji sovmen (2017) - IMDb.html"), indent=4))
    print(json.dumps(extract_imdb("../input-extraction/imdb.com/PK (2014) - IMDb.html"), indent=4))

elif sys.argv[1] == "B":
    print(extract_overstock_xpath("../input-extraction/overstock.com/jewelry01.html"))
    print(extract_overstock_xpath("../input-extraction/overstock.com/jewelry02.html"))
    print(extract_rtvslo_xpath("../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html"))
    print(extract_rtvslo_xpath("../input-extraction/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljs╠îe v razredu - RTVSLO.si.html"))
    print(extract_imdb_xpath("../input-extraction/imdb.com/Najvecji sovmen (2017) - IMDb.html"))
    print(extract_imdb_xpath("../input-extraction/imdb.com/PK (2014) - IMDb.html"))

elif sys.argv[1] == "C":
    print(road_runner("../input-extraction/overstock.com/jewelry01.html", "../input-extraction/overstock.com/jewelry02.html"))
    print(road_runner("../input-extraction/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html",
                      "../input-extraction/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljs╠îe v razredu - RTVSLO.si.html"))
    print(road_runner("../input-extraction/imdb.com/Najvecji sovmen (2017) - IMDb.html",
                      "../input-extraction/imdb.com/PK (2014) - IMDb.html"))