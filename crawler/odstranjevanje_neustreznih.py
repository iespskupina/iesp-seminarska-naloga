from urllib.parse import urlparse

import psycopg2

import pajek.nastavitve as conf
from pajek.frontierCotroller import pridobiPodatkeNaslova
from pajek.podobnostni_hash import podobnostni_hash
from pajek.splet.robotsParser import RobotParser

conn = None

def connect_to_db():
    """ Return connection to database """
    global conn
    if conn is None:
        conn = psycopg2.connect(host=conf.host, user=conf.user, password=conf.password, port=conf.port)
        conn.autocommit = True
    return conn

if __name__ == '__main__':
    conn = connect_to_db()

    cur = conn.cursor()

    cur.execute("SELECT id, site_id, url FROM crawldb.page WHERE page_type_code = 'FRONTIER'")
    rezultat = cur.fetchall()
    st_odstranjenih = 0
    prejsnji_procent = 0
    i = 0
    for r in rezultat:
        i+=1
        if int(100*i/len(rezultat)) > prejsnji_procent:
            prejsnji_procent = int(100*i/len(rezultat))
            print(prejsnji_procent)
        url = r[2]
        parsed_uri = urlparse(url)
        osnova_domene = parsed_uri.netloc
        pot = parsed_uri.path
        # print(url)
        robots = pridobiPodatkeNaslova(url)[2]
        robots_parser = RobotParser(robots)
        if not robots_parser.dovoljenDostop(pot):
            st_odstranjenih += 1
            # cur.execute("DELETE FROM crawldb.page WHERE id = %s", (r[0],))

    print(st_odstranjenih)

    cur.close()
    conn.close()
