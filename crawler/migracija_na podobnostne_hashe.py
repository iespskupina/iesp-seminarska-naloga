import psycopg2

import pajek.nastavitve as conf
from pajek.podobnostni_hash import podobnostni_hash

conn = None

def connect_to_db():
    """ Return connection to database """
    global conn
    if conn is None:
        conn = psycopg2.connect(host=conf.host, user=conf.user, password=conf.password, port=conf.port)
        conn.autocommit = True
    return conn

if __name__ == '__main__':
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("ALTER TABLE crawldb.page ADD COLUMN similarity_hash TEXT;")
    cur.execute("CREATE INDEX \"idx_page_site_similarity_hash\" ON crawldb.page USING gist(similarity_hash gist_trgm_ops);")

    nadaljuj = True
    while nadaljuj:
        cur.execute("SELECT * FROM crawldb.page WHERE html_content != '' AND (similarity_hash = '') IS NOT FALSE LIMIT 1")
        rezultat = cur.fetchone()
        if rezultat is None:
            nadaljuj = False
            break
        print("obdelujem id:", rezultat[0])
        cur.execute("UPDATE crawldb.page SET similarity_hash = %s WHERE id = %s", (podobnostni_hash(rezultat[4]), rezultat[0]))

    cur.close()
    conn.close()
