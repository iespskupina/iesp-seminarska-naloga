import pajek.baza.poizvedbe
from pajek.baza import poizvedbe
from pajek.baza.write_to_db import insert_link
from pajek.pajek import zazeni_raziskovanje, dodaj_zacetne_strani

poizvedbe.reset_db()

dodaj_zacetne_strani()

zazeni_raziskovanje()
