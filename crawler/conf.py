import configparser

# read config.INI file
config = configparser.ConfigParser()
try:
    config.read("../../config/config.INI")
    db = config['database']
    crawler = config['crawler']
except Exception as e:
    pass

try:
    config.read("../config/config.INI")
    db = config['database']
    crawler = config['crawler']
except Exception as e:
    pass

try:
    config.read("config/config.INI")
    db = config['database']
    crawler = config['crawler']
except Exception as e:
    pass


host = db['host']
database = db['database']
user = db['user']
password = db['password']

threads = crawler['threads']
user_agent = crawler['user_agent']
time_per_request = crawler['time_per_request']
