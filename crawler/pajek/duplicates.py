import hashlib
from datetime import datetime
from urllib.request import urlopen
import psycopg2
import pajek.nastavitve as conf
from pajek.podobnostni_hash import podobnostni_hash

conn = None

def connect_to_db():
    """ Return connection to database """
    global conn
    if conn is None:
        conn = psycopg2.connect(host=conf.host, user=conf.user, password=conf.password, port=conf.port)
        conn.autocommit = True
    return conn


def get_url(url):
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("SELECT url FROM crawldb.page WHERE url = %s", (url,))
    result = cur.fetchall()

    cur.close()

    url = ""
    if result != []:
        url = result[0][0]

    return url


def get_site_id(url):
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("SELECT site_id FROM crawldb.page WHERE url = %s", (url,))
    result = cur.fetchall()

    cur.close()

    return result[0][0]


def get_all_content(url):
    conn = connect_to_db()

    site_id = get_site_id(url)

    cur = conn.cursor()
    cur.execute("SELECT html_content FROM crawldb.page WHERE html_content != '' AND site_id = %s", (site_id, ))
    result = cur.fetchall()

    cur.close()

    html_contents = [content[0] for content in result]

    return html_contents


def get_duplicate_page_id(url):
    """ Return page id """
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("SELECT id FROM crawldb.page WHERE url = %s", (url,))
    result = cur.fetchall()

    cur.close()

    return result[0][0]


def check_duplicate(hash_content):
    """ Check if hash_content is equal to some hash in database """
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("SELECT id FROM crawldb.page WHERE hash = %s", (hash_content,))
    result = cur.fetchall()

    cur.close()

    if result != []:
        return result[0][0]

    return False


def get_hash(content):
    return hashlib.md5(content.encode('utf-8')).hexdigest()


def is_duplicate(url):
    """ Return True if url is duplicate else return False """

    url = get_url(url)

    # check if url is the same
    if url != "":
        return True

    return False


def check_similarity(content, url):
    """ Check similarity between new content and contents in database, if similarity > 0.9 return id of duplicate page else False """
    conn = connect_to_db()

    cur = conn.cursor()
    site_id = get_site_id(url)

    cur.execute("SELECT url, similarity(similarity_hash, %s) as sim FROM crawldb.page WHERE html_content != '' AND site_id = %s ORDER BY sim DESC LIMIT 1", (podobnostni_hash(content), site_id))
    result = cur.fetchall()

    cur.close()

    if result != []:
        if result[0][1] > 0.90:
            id = get_duplicate_page_id(result[0][0])
            return id

    return False


def jaccard_distance(x, y):
    return len(x & y) / len(x | y)


def get_pieces(content, param=5):
    n = len(content)
    return set(content[i:i + param] for i in range(0, n - param))


def is_duplicate_lsh(url, content):
    """ Return page id if it is duplicate else return False """

    hash_content = get_hash(content)
    id_duplicate = check_duplicate(hash_content)

    if id_duplicate:
        return id_duplicate

    id_duplicate = check_similarity(content, url)

    if id_duplicate:
        return id_duplicate

    return False


    # url_contents = get_all_content(url)
    #
    # # check if content is the same
    # hash_urls = set()
    # for v in url_contents:
    #     hash_urls.add(get_hash(v))
    #
    # h = get_hash(content)
    #
    # if h in hash_urls:
    #     return get_duplicate_page_id(url)
    #
    # all = []
    #
    # for i in url_contents:
    #     all.append(get_pieces(i.lower()))
    #
    # s = get_pieces(content.lower())
    #
    # for i in all:
    #     prob = jaccard_distance(i, s)
    #     is_dup = prob >= 0.9
    #     if is_dup:
    #         return get_duplicate_page_id(url)
    #
    # return False


if __name__ == "__main__":

    url = "https://e-uprava.gov.si/podrocja/nepremicnine-in-okolje.html?view_mode=3"
    u = urlopen(url).read()
    content = u.decode('utf-8')

    print(is_duplicate_lsh(url, content))



