from urllib.parse import urlparse

from pajek.baza.poizvedbe import dodajDomeno, aliJeStranZeDodana
from pajek.splet import robotsParser


parser = None

def pridobiPodatkeNaslova(url):
    parsed_uri = urlparse(url)
    osnova_domene = parsed_uri.netloc
    return dodajDomeno(osnova_domene)

def lahkoDodamVFrontier(url):
    global parser
    parsed_uri = urlparse(url)
    osnova_domene = parsed_uri.netloc
    pot = parsed_uri.path

    if not osnova_domene.endswith("gov.si"):
        return False

    robots = pridobiPodatkeNaslova(url)[2]
    if robots != "":
        if (pot == ""):
            pot = "/"
        if parser == None:
            parser = robotsParser.RobotParser(robots)
        else:
            parser.nastaviVsebino(robots)
        if not parser.dovoljenDostop(pot):
            return False

    if aliJeStranZeDodana(url):
        return False

    return True

