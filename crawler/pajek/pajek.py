import concurrent.futures
import socket
import sys
import threading
import traceback
from urllib.parse import urlparse
from http.server import HTTPServer, BaseHTTPRequestHandler
import time

import pajek.baza.poizvedbe
from pajek import nastavitve
from pajek.baza.write_to_db import insert_link, insert_new_link
import pajek.nastavitve as conf
from pajek.splet.web_selenium import obdelajStran

zadnji_dostop = {}
aktivni_raziskovalci = []

lock = threading.Lock()

stanje_raziskovalcev = []

neuspesne_strani = []
st_uspesnih_strani = 0

nadaljuj = True

def raziskovalec():
    global st_uspesnih_strani, neuspesne_strani, stanje_raziskovalcev, zadnji_dostop, aktivni_raziskovalci, nadaljuj
    id_raziskovalca = 0
    with lock:
        id_raziskovalca = len(aktivni_raziskovalci)
        aktivni_raziskovalci.append(True)
        stanje_raziskovalcev.append("Zacet")
    print(id_raziskovalca, "Zacenjam")
    while nadaljuj in aktivni_raziskovalci:
        stanje_raziskovalcev[id_raziskovalca] = "Cakam da iz baze dobim novo stran"
        # Najdi neobdelano stran
        try:
            (idStrani, urlStrani) = pajek.baza.poizvedbe.pridobiStranZaObdelavo()
        except:
            print(id_raziskovalca, "Napaka pri pridobivanju strani")
            urlStrani = ""
            idStrani = -1
        if urlStrani != "":
            try:
                stanje_raziskovalcev[id_raziskovalca] = "Priprava na obdelavo strani: " + urlStrani
                if (not aktivni_raziskovalci[id_raziskovalca]):
                    print(id_raziskovalca, "Zbudim se")
                aktivni_raziskovalci[id_raziskovalca] = True
                parsed_uri = urlparse(urlStrani)
                domena = parsed_uri.netloc
                ip = socket.gethostbyname(domena)
                if ip in zadnji_dostop:
                    while zadnji_dostop[ip] + int(conf.time_per_request) > time.time():
                        stanje_raziskovalcev[id_raziskovalca] = "Čakam da lahko dostopam do ip: " + ip
                        time.sleep(0.1)
                zadnji_dostop[ip] = time.time()
            except:
                print(id_raziskovalca, "Napaka pri pripravi na obdelavo strani", urlStrani)
            try:
                stanje_raziskovalcev[id_raziskovalca] = "Obdelujem stran: " + urlStrani
                # print(id_raziskovalca, "Obdelujem stran:", urlStrani)
                obdelajStran(urlStrani, idStrani)
                st_uspesnih_strani += 1
                stanje_raziskovalcev[id_raziskovalca] = "Koncana stran: " + urlStrani
                # print(id_raziskovalca, "Koncana obdelava:", urlStrani)
            except:
                print(id_raziskovalca, "Napaka pri obdelavi strani", urlStrani)
                stanje_raziskovalcev[id_raziskovalca] = "Napaka pri obdelavi: " + urlStrani
                neuspesne_strani.append(urlStrani)
                print(traceback.format_exc())
                try:
                    pajek.baza.poizvedbe.nastaviNapako(idStrani)
                except:
                    print(id_raziskovalca, "Napaka pri shranjevanju napake", urlStrani)


        else:
            try:
                if (aktivni_raziskovalci[id_raziskovalca]):
                    print(id_raziskovalca, "Spim")
                stanje_raziskovalcev[id_raziskovalca] = "Cakam da se v bazi pojavi tip FRONTIER"
                aktivni_raziskovalci[id_raziskovalca] = False
                time.sleep(10)
            except:
                print(id_raziskovalca, "Napaka pri spanju")

    stanje_raziskovalcev[id_raziskovalca] = "Ustavljen"


def dodaj_zacetne_strani():
    for stran in nastavitve.sites:
        insert_new_link("FRONTIER", stran)


def zazeni_status_streznik():
    httpd = HTTPServer(('localhost', 8000), PajekStatusStreznik)
    httpd.serve_forever()


def zazeni_raziskovanje():
    pajek.baza.poizvedbe.add_page_type("INPROGRESS")
    pajek.baza.poizvedbe.add_page_type("ERROR")
    thread = threading.Thread(target=zazeni_status_streznik)
    thread.start()
    # raziskovalec()
    with concurrent.futures.ThreadPoolExecutor(max_workers=int(conf.threads)) as executor:
        for _ in range(int(conf.threads)):
            executor.submit(raziskovalec)


class PajekStatusStreznik(BaseHTTPRequestHandler):
    def do_GET(self):
        global nadaljuj
        if self.path == "/ustavi":
            nadaljuj = False
        global stanje_raziskovalcev, st_uspesnih_strani, neuspesne_strani
        self.send_response(200)
        self.end_headers()
        odgovor = "<html><head><meta charset=\"UTF-8\"><style>table, th, td {border: 1px solid black;}</style></head><body><table><tr>"
        for i in range(len(stanje_raziskovalcev)):
            odgovor += "<th>" + str(i) + "</th>"

        odgovor += "</tr><tr>"

        for status in stanje_raziskovalcev:
            odgovor += "<td>" + status + "</td>"
        odgovor += "</tr></table>"

        if not nadaljuj:
            odgovor+="<p>Ustavljanje</p>"
        odgovor += "<p>Uspešne strani: " + str(st_uspesnih_strani) + " Neuspešne strani: " + str(
            len(neuspesne_strani)) + "</p><p>Neuspešne strani:</p>"

        for stran in neuspesne_strani:
            odgovor += "<span>" + stran + "<br></span>"

        odgovor += "</body></html>"

        self.wfile.write(odgovor.encode('utf-8'))


if __name__ == '__main__':
    insert_new_link("FRONTIER", "https://www.google.com")
    zazeni_raziskovanje()
