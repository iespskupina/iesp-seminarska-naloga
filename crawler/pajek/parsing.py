from bs4 import BeautifulSoup
import urllib
import urllib.request
import re
from url_normalize import url_normalize
from urllib.parse import urlparse
from urllib.parse import urljoin
import validators

def is_absolute(url):
    return bool(urlparse(url).netloc)

def parse_link(ref, current_link):
    if ref:
        if not is_absolute(ref):
            ref = urljoin(current_link, ref)
        if re.search("\.gov\.si$", str(urlparse(ref).hostname)):
            return url_normalize(ref)

def get_all_links(content, current_link):
    soup = BeautifulSoup(content, "html.parser")
    # print(soup.prettify())
    links = []


    try:
        baseurl = soup.findAll("base")[0]
        if baseurl:
            current_link = baseurl["href"]
            print(current_link)
    except:
        pass


    #find all href
    for link in soup.findAll('a'):
        try:
            ref = link.get('href')
            parsed_link = parse_link(ref, current_link)
            if parsed_link:
                links.append(parsed_link)
        except:
            pass

    #find all location.href in a and button
    for link in soup.findAll(lambda tag: (tag.name == "a" or tag.name == "button") and
                tag.has_attr("onclick") and
                "location.href" in tag["onclick"] ):
        try:
            ref = re.findall('(?<=location\.href=).*$', link["onclick"])[0]
            if ref.startswith("\'") or ref.startswith("\""):
                ref = ref[1:]
            if ref.endswith("\'") or ref.endswith("\""):
                ref = ref[:-1]

            parsed_link = parse_link(ref, current_link)
            if parsed_link:
                links.append(parsed_link)
        except:
            pass

    # find all document.location in a and button
    for link in soup.findAll(lambda tag: (tag.name == "a" or tag.name == "button") and
                tag.has_attr("onclick") and
                "document.location" in tag["onclick"]):
        try:
            ref = re.findall('(?<=document\.location).*$', link)[0].split("=")[1]
            if ref.startswith("\'") or ref.startswith("\""):
                ref = ref[1:]
            if ref.endswith("\'") or ref.endswith("\""):
                ref = ref[:-1]

            parsed_link = parse_link(ref, current_link)
            if parsed_link:
                links.append(parsed_link)
        except:
            pass
    return links

def get_all_images(content, current_link):
    soup = BeautifulSoup(content, "html.parser")
    images = []

    for img in soup.findAll('img'):
        ref = img["src"]
        if not is_absolute(ref):
            ref = urljoin(current_link, ref)
            if not validators.url(ref):
                continue
        try:
            type = img["src"].split(".")
            if len(type) == 1 or len(type[-1])>50:
                images.append((img["src"], "unknown"))
            else:
                images.append((img["src"], type[-1]))
        except:
            images.append((img["src"], "unknown"))
    return images


if __name__ == "__main__":

    url = "https://gov.si"
    content = urllib.request.urlopen(url).read()
    links = get_all_links(content, url)
    images = get_all_images(content, url)
    print(links)
    print(images)
