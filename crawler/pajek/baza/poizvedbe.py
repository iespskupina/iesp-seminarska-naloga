import concurrent.futures
import configparser
import threading
import urllib.request
from urllib.robotparser import RobotFileParser

import psycopg2

import pajek.nastavitve as conf
from pajek.splet.robotsParser import RobotParser

lock = threading.Lock()

conn = None


def connect_to_db():
    """ Return connection to database """
    global conn
    if conn is None:
        conn = psycopg2.connect(host=conf.host, user=conf.user, password=conf.password, port=conf.port)
        conn.autocommit = True
    return conn


def pripravi_tabele():
    with open("crawldb.sql") as f:
        content = f.readlines()
    conn = connect_to_db()
    with lock:
        cur = conn.cursor()
        cur.execute("".join(content))
        cur.close()


def dodajDomeno(domena):
    conn = connect_to_db()
    rezultat = []
    with lock:
        cur = conn.cursor()
        cur.execute("SELECT * FROM crawldb.site WHERE domain = %s", (domena,))
        rezultat = cur.fetchall()
        if (len(rezultat) == 0):
            # pridobi robots.txt in sites.txt
            sites = ""
            try:
                robots = urllib.request.urlopen("http://" + domena + "/robots.txt").read().decode("utf-8")
            except:
                robots = ""
            if robots != "":
                parser = RobotParser(robots)
                sitesLocation = parser.site_map()
                if sitesLocation != "":
                    try:
                        sites = urllib.request.urlopen(sitesLocation).read().decode("utf-8")
                    except:
                        sites = ""

            cur.execute("INSERT INTO crawldb.site(domain, robots_content, sitemap_content) VALUES (%s, %s, %s)",
                        (domena, robots, sites))
            cur.execute("SELECT * FROM crawldb.site WHERE domain = %s", (domena,))
            rezultat = cur.fetchall()
        cur.close()
    return rezultat[0]


def aliJeStranZeDodana(naslov):
    conn = connect_to_db()
    cur = conn.cursor()
    cur.execute("SELECT id FROM crawldb.page WHERE url = %s", (naslov,))
    rezultat = cur.fetchall()
    cur.close()
    return len(rezultat) > 0


def pridobiStranZaObdelavo():
    conn = connect_to_db()
    idStrani = -1
    urlStrani = ""

    with lock:
        cur = conn.cursor()
        cur.execute("SELECT id, site_id, page_type_code, url FROM crawldb.page WHERE page_type_code = %s ORDER BY random() LIMIT 1", ("FRONTIER",))
        rezultat = cur.fetchone()
        if rezultat is not None:
            cur.execute("UPDATE crawldb.page SET page_type_code = %s WHERE id = %s", ("INPROGRESS", rezultat[0]))
            urlStrani = rezultat[3]
            idStrani = rezultat[0]
        cur.close()
    return (idStrani, urlStrani)


def nastaviNapako(idStrani):
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("UPDATE crawldb.page SET page_type_code = %s WHERE id = %s", ("ERROR", idStrani))
    cur.close()


def add_page_type(tip):
    conn = connect_to_db()
    cur = conn.cursor()
    cur.execute("SELECT * FROM crawldb.page_type WHERE code = %s LIMIT 1", (tip,))
    if cur.fetchone() is None:
        cur.execute("INSERT INTO crawldb.page_type(code) VALUES (%s)", (tip,))
    cur.close()

def reset_db():
    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("DELETE FROM crawldb.image")
    cur.execute("DELETE FROM crawldb.link")
    cur.execute("DELETE FROM crawldb.page_data")
    cur.execute("DELETE FROM crawldb.page")
    cur.execute("DELETE FROM crawldb.site")
    cur.close()


if __name__ == '__main__':
    print(pridobiStranZaObdelavo())
