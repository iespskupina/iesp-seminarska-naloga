import hashlib
import threading
import psycopg2
import tldextract as tldextract
from pajek.frontierCotroller import pridobiPodatkeNaslova
from urllib.parse import urlparse
import pajek.nastavitve as conf
from pajek.podobnostni_hash import podobnostni_hash

lock = threading.Lock()

conn = None

def connect_to_db():
    """ Return connection to database """
    global conn
    if conn is None:
        conn = psycopg2.connect(host=conf.host, user=conf.user, password=conf.password, port=conf.port)
        conn.autocommit = True
    return conn


def get_domain(url):
    """ Return url's domain """
    parsed_uri = urlparse(url)
    osnova_domene = parsed_uri.netloc
    return osnova_domene


def get_site_id(url):
    """ Return site_id """
    site = pridobiPodatkeNaslova(url)
    site_id = site[0]
    return site_id


def get_page_id(page_type_code, url, html_content, http_status_code, accessed_time):
    """ Return page_id """
    conn = connect_to_db()
    cur = conn.cursor()

    cur.execute("SELECT id FROM crawldb.page WHERE page_type_code = %s and url = %s and html_content = %s and"
                " http_status_code = %s and accessed_time = %s",
                (page_type_code, url, html_content, http_status_code, accessed_time))
    page_id = cur.fetchall()[0][0]

    cur.close()

    return page_id


def get_page_id_from_url(url):
    """ Return page_id """
    conn = connect_to_db()
    cur = conn.cursor()

    cur.execute("SELECT id FROM crawldb.page WHERE url = %s",
                (url, ))
    page_id = cur.fetchall()[0][0]

    cur.close()

    return page_id


def is_html(page_type_code):
    """ Check if page type is HTML """
    if page_type_code == "HTML":
        return True
    return False


def get_hash(content):
    return hashlib.md5(content.encode('utf-8')).hexdigest()


def update_link(page_type_code, url, html_content, http_status_code, accessed_time, data_type_code):
    """ Update link into table page (if page_type_code != HTML insert also in page_data) """

    conn = connect_to_db()

    with lock:
        cur = conn.cursor()

        site_id = get_site_id(url)
        hash_content = None
        if html_content is not None:
            hash_content = get_hash(html_content)

        podobnostni_hash_vrednost = None
        if html_content is not None:
            podobnostni_hash_vrednost = podobnostni_hash(html_content)

        if page_type_code == "BINARY":
            # if page_code_type is binary
            html_content = None
            cur.execute("UPDATE crawldb.page SET page_type_code = %s, html_content = %s, http_status_code = %s,"
                        "accessed_time = %s, hash = %s, similarity_hash = %s WHERE url = %s",
                        (page_type_code, html_content, http_status_code, accessed_time, hash_content, podobnostni_hash_vrednost, url))

            # save in page_data if page is binary (pdf/ppt/doc..)
            page_id = get_page_id_from_url(url)
            cur.execute("INSERT INTO crawldb.page_data(page_id, data_type_code, data) "
                        "VALUES (%s, %s, %s)", (page_id, data_type_code, None))
        else:
            # if page_type_code is html update all values
            cur.execute("UPDATE crawldb.page SET page_type_code = %s, html_content = %s, http_status_code = %s,"
                        "accessed_time = %s, hash = %s, similarity_hash = %s WHERE url = %s",
                        (page_type_code, html_content, http_status_code, accessed_time, hash_content, podobnostni_hash_vrednost, url))
        cur.close()


def insert_new_link(page_type_code, url):
    """ Insert new link into table page (if page_type_code != HTML insert also in page_data) """

    conn = connect_to_db()

    with lock:
        cur = conn.cursor()

        site_id = get_site_id(url)
        cur.execute("INSERT INTO crawldb.page(site_id, page_type_code, url, html_content, http_status_code, "
                    "accessed_time, hash, similarity_hash) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                    (site_id, page_type_code, url, None, None, None, None, None))

        cur.execute("SELECT id FROM crawldb.page WHERE url = %s", (url,))
        result = cur.fetchall()

        cur.close()
    return result[0][0]


def insert_image(page_id, filename, content_type, data, accessed_time):
    """ Insert new image into table image """

    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("INSERT INTO crawldb.image(page_id, filename, content_type, data, accessed_time) "
                "VALUES (%s, %s, %s, %s, %s)", (page_id, filename, content_type, data, accessed_time))
    cur.close()


def insert_link(from_page, to_page):
    """ Insert new connection between pages into table link """

    conn = connect_to_db()

    cur = conn.cursor()
    cur.execute("INSERT INTO crawldb.link(from_page, to_page) VALUES (%s, %s)", (from_page, to_page))
    cur.close()


def set_duplicate(url, page_type_code, http_status_code, accessed_time):
    """ Set duplicate in table page """
    conn = connect_to_db()
    cur = conn.cursor()

    cur.execute("UPDATE crawldb.page SET page_type_code = %s, html_content = %s, http_status_code = %s,"
                "accessed_time = %s, hash = %s,  similarity_hash = %s WHERE url = %s",
                (page_type_code, None, http_status_code, accessed_time, None, None, url))

    cur.close()


if __name__ == "__main__":
    get_domain("https://gov.si/")