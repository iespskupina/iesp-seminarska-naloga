host = "localhost"
database = "postgres"
user = "user"
password = "SecretPassword"
port = "5432"

sites=["https://www.gov.si/", "https://evem.gov.si", "https://e-uprava.gov.si", "https://e-prostor.gov.si"]

threads = 1
user_agent = "fri-ieps-TKT"
time_per_request = 4
timeout = 5
web_driver = "/home/timotej/Programi/webdriver/chromedriver"
