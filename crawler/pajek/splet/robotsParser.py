import configparser
import re
import pajek.nastavitve as conf

class RobotParser:
    def __init__(self, vsebina):
        self.vsebina = vsebina

    def nastaviVsebino(self, vsebina):
        self.vsebina = vsebina

    def dovoljenDostop(self, pot):
        vrstice = self.vsebina.split("\n")
        veljaZaNas = True
        izbranaPot = ""
        izbranoDovoljenje = True
        for vrstica in vrstice:
            # Ce se vrstica zacne z # jo preskocimo
            if re.search(r' *#.*', vrstica, re.I) is not None:
                continue

            # Ali vrstica opisuje user agenta
            rezultati = re.search(r'User-agent ?: ?([a-zA-Z0-9\\*-]+)', vrstica, re.I)
            if rezultati is not None:
                agent = rezultati.group(1)
                veljaZaNas = (agent == "*" or agent == conf.user_agent)

            if veljaZaNas:
                rezultati = re.search(r'(Allow|Disallow) ?: ?(\S+)$', vrstica, re.I)
                if rezultati is not None:
                    potPravila = rezultati.group(2)
                    regex_pot_pravila = "^" + potPravila.replace(".", "\\.").replace("-", "\\-").replace("*", ".*")
                    regex_ujemanje = re.search(re.compile(regex_pot_pravila), pot)
                    if (regex_ujemanje is not None and len(potPravila) > len(izbranaPot)):
                        izbranaPot = potPravila
                        izbranoDovoljenje = rezultati.group(1) == "Allow"

        return izbranoDovoljenje

    def site_map(self):
        rezultati = re.search(r'Sitemap ?: ?([a-zA-Z0-9\\-_\\.~?/:=]+)', self.vsebina, re.I)
        if rezultati is not None:
            return rezultati.group(1)
        else:
            return ""

# if __name__ == '__main__':
#     robot = RobotParser("User-Agent: *\n\
# Disallow: /s/\n\
# Disallow: /snippets/new\n\
# Disallow: /snippets/*/edit\n\
# Disallow: /snippets/*/raw\n\
# ")
#     print(robot.dovoljenDostop("/sdf/s/nippets/lsl/rawsdf"))