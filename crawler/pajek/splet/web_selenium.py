import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import pajek.baza.write_to_db as db
from datetime import datetime
from pajek.parsing import get_all_images, get_all_links
from pajek.duplicates import is_duplicate, is_duplicate_lsh
import pajek.nastavitve as conf
from pajek.frontierCotroller import lahkoDodamVFrontier

def obdelajStran(url, page_id):

    accessed_time = datetime.now()
    response = requests.get(url, timeout=15, stream=True)
    content_type = response.headers["Content-Type"]
    status_code = response.status_code

    # print(response)
    # print(content_type, status_code)

    if "text/html" in content_type:
        if status_code > 399:
            db.update_link("HTML", url, None, status_code, accessed_time, None)
        else:
            html = get_html_content(url)

            duplicate_id = is_duplicate_lsh(url, html)
            if duplicate_id:
                db.set_duplicate(url, "DUPLICATE", status_code, accessed_time)
                db.insert_link(page_id, duplicate_id)
            else:
                db.update_link("HTML", url, html, status_code, accessed_time, None)

                links = get_all_links(html, url)
                for link in links:
                    if lahkoDodamVFrontier(link):
                        #try to figure document type from extension
                        if link.endswith(".doc"):
                            new_id = db.insert_new_link("BINARY", link)
                            db.update_link("BINARY", link, None, status_code, accessed_time, "DOC")
                        elif link.endswith(".docx"):
                            new_id = db.insert_new_link("BINARY", link)
                            db.update_link("BINARY", link, None, status_code, accessed_time, "DOCX")
                        elif link.endswith(".pdf"):
                            new_id = db.insert_new_link("BINARY", link)
                            db.update_link("BINARY", link, None, status_code, accessed_time, "PDF")
                        elif link.endswith(".ppt"):
                            new_id = db.insert_new_link("BINARY", link)
                            db.update_link("BINARY", link, None, status_code, accessed_time, "PPT")
                        elif link.endswith(".pptx"):
                            new_id = db.insert_new_link("BINARY", link)
                            db.update_link("BINARY", link, None, status_code, accessed_time, "PPTX")
                        else:
                            new_id = db.insert_new_link("FRONTIER", link)
                        db.insert_link(page_id, new_id)


                images = get_all_images(html, url)
                for image, type in images:
                    db.insert_image(page_id, image, type, None, accessed_time)

    elif "application/msword" in content_type:
        db.update_link("BINARY", url, None, status_code, accessed_time, "DOC")
    elif "application/vnd.openxmlformats-officedocument.wordprocessingml.document" in content_type:
        db.update_link("BINARY", url, None, status_code, accessed_time, "DOCX")
    elif "application/pdf" in content_type:
        db.update_link("BINARY", url, None, status_code, accessed_time, "PDF")
    elif "application/vnd.ms-powerpoint" in content_type:
        db.update_link("BINARY", url, None, status_code, accessed_time, "PPT")
    elif "application/vnd.openxmlformats-officedocument.presentationml.presentation" in content_type:
        db.update_link("BINARY", url, None, status_code, accessed_time, "PPTX")
    else:
        print("binary")
        db.update_link("BINARY", url, None, status_code, accessed_time, None)


def get_html_content(url):
    chrome_options = Options()
    chrome_options.add_argument("--headless")

    chrome_options.add_argument("user-agent=" + conf.user_agent)
    driver = webdriver.Chrome(conf.web_driver,
                              options=chrome_options)
    driver.get(url)
    time.sleep(int(conf.timeout))
    html = driver.page_source
    driver.close()
    driver.quit()

    return html

def select_all():
    """ Return page_id """
    conn = db.connect_to_db()
    cur = conn.cursor()

    cur.execute("SELECT id, page_type_code, url, http_status_code FROM crawldb.page")
    # cur.execute("SELECT * FROM crawldb.page")
    print(cur.fetchall())

    cur.close()

def select_binary():
    """ Return page_id """
    conn = db.connect_to_db()
    cur = conn.cursor()

    cur.execute("SELECT id, page_type_code, url, http_status_code FROM crawldb.page WHERE page_type_code = %s", ("BINARY", ))
    # cur.execute("SELECT * FROM crawldb.page")
    print(cur.fetchall())

    cur.close()

def select_pagedata(id):
    """ Return page_id """
    conn = db.connect_to_db()
    cur = conn.cursor()

    cur.execute("SELECT * FROM crawldb.page_data WHERE page_id = %s", (id,))
    # cur.execute("SELECT * FROM crawldb.page")
    print(cur.fetchall())

    cur.close()

def delete():
    """ Return page_id """
    conn = db.connect_to_db()
    cur = conn.cursor()

    cur.execute("DELETE FROM crawldb.link")
    cur.execute("DELETE FROM crawldb.image")
    cur.execute("DELETE FROM crawldb.page_data")
    cur.execute("DELETE FROM crawldb.page")
    cur.execute("DELETE FROM crawldb.site")



    cur.close()


if __name__ == "__main__":
    select_all()
    # delete()
    id = db.insert_new_link("FRONTIER", "http://www.arrs.gov.si")
    select_all()
    obdelajStran("http://www.arrs.gov.si", id)
    # obdelajStran("http://aka.ms/windev_VM_virtualbox", 1)
    # obdelajStran("https://ucilnica.fri.uni-lj.si/pluginfile.php/135998/mod_resource/content/1/Zapiski1.pdf", 1)
    # obdelajStran("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf", 1)
    # # obdelajStran("http://www.pisrs.si/Pis.web/pregledPredpisa?id=ZAKO6571/npbDocPdf?idPredpisa=ZAKO7794&idPredpisaChng=ZAKO6571&type=pdf")
    select_all()
    # select_pagedata(252)