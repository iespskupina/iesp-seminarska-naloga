import math
import re
import urllib.request
from math import ceil


def podobnostni_hash(vhod):
    vhod = vhod.replace("\n", "").replace("\r", "").replace("\t", "")
    vhod = re.sub('<head.*?</head>|<svg.*?</svg>|<script.*?</script>', '', vhod)
    velikost_izhoda = 500
    deli_vhoda = re.split("</p>|</span>|</div>|</head>|</body>|<br ?/?>", vhod)
    while len(deli_vhoda) > velikost_izhoda:
        deli_vhoda = [deli_vhoda[i*2] + deli_vhoda[i*2+1] for i in range(math.floor(len(deli_vhoda)/2))]
    dolzinaDela = velikost_izhoda / len(deli_vhoda)
    izhod = ""
    for del_vhoda in deli_vhoda:
        izhod += obdelaj_del(del_vhoda, int(dolzinaDela))

    if izhod == "":
        izhod = "Prazno"
    # print(izhod)
    return izhod

def obdelaj_del(vhod, velikost_izhoda):
    base64_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    biti = vhod.encode("utf-8")
    if len(biti) <= velikost_izhoda:
        return vhod
    bajtov_na_znak = ceil(len(biti) / velikost_izhoda)
    # print(bajtov_na_znak)
    izhod = ""
    for i in range(velikost_izhoda):
        int_izhod = 0
        zacetek_bloka = int(i * len(biti) / velikost_izhoda)
        for j in range(bajtov_na_znak):
            if i * bajtov_na_znak + j >= len(biti):
                break
            int_izhod += biti[zacetek_bloka + j]
            int_izhod = int_izhod % 64
        izhod += base64_letters[int_izhod]
    return izhod

if __name__ == '__main__':
    stran1 = urllib.request.urlopen("http://www.fu.gov.si/nadzor/podrocja/financni_nadzor/novica/furs_ugotavlja_nepravilnosti_pri_obracunavanju_ddv_pri_izvajanju_kurirske_dejavnosti_v_povezavi_s_stalnimi_poslovnimi_enotami_in_napoveduje_poostren_nadzor_9375/").read().decode("utf-8")
    stran2 = urllib.request.urlopen("http://www.fu.gov.si/carina/poslovanje_z_nami/e_carina/novica/vabilo_na_sestanek_furs_s_ponudniki_programske_opreme_za_elektronsko_poslovanje_gospodarskih_subjektov_s_furs_pri_izvajanju_carinskih_formalnosti_9478/").read().decode("utf-8")
    print(len(stran1))
    print(len(stran2))
    # print(stran1[1292:2584])
    # print(stran2[1292:2584])
    print(podobnostni_hash(stran1))
    print(podobnostni_hash(stran2))
