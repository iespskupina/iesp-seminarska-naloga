# Projekt
Pri predmetu Iskanje in ekstrakcija podatkov s spleta na Fakulteti za računalštvo in informatiko je bila izdelana seminarska naloga, sestavljena iz 3 delov.
V prvem delu je bil izdelan pajek, v drugem je bila izdelana ekstrakcija podatkov s spleta.
Avtorji projekta so Timotej Knez, Tara Patricija Bosil in Katja Logar.

## Spletni pajek
### Opis 
Pri prvem delu projekta je bil izdelan pajek za pridobivanje strani iz domene gov.si. 
Začetne strani so bile gov.si, evem.gov.si, e-uprava.gov.si in e-prostor.gov.si. 

### Priprava in zagon
Pajek potrebuje postgres podatkovno bazo.

1. Podatke za prijavo v bazo je potrebno vnesti v datoteko `pajek/nastavitve.py`.
2. Na računalnik je potrebno naložiti chrome driver in v `pajek/nastavitve.py` nastaviti pot do njega.
3. Namestiti je potrebno vse knjižnice v datoteki `requirements.txt`. (To je mogoče storiti z ukazom `pip install -r requirements.txt`)
4. V bazi mora biti nameščen dodatek `pg_trgm`
5. Za pripravo baze je potrebno zagnati `setup.py`
6. Za zagon pajka je potrebno zagnati `main.py` (`main.py` tudi pobriše vse vrstice iz baze)

### Nadzorovanje pajka
Med pregledovanjem spleta pajek na vratih `8000` odpre preprost spletni strežnik, ki prikazuje, kaj trenutno dela vsaka izmed niti.

## Ekstrakcija podatkov s spleta

### Opis
Pri drugem delu projekta je bila izdelana ekstrakcija podatkov iz 3 domen: Overstock, Rtvslo.si in IMDB.
Za vsako domeno smo uporabili 2 spletni strani.
Na spodnji sliki so tudi predstavljeni podatki, ki smo jih ekstrahirali iz spletne strani IMDB.

![alt text](./input-extraction/imdb.com/imdb.png)


### Priprava in zagon
1. Potrebno je namestiti knjižnice: `re`, `json`, `lxml` in `io`.
2. Za zagon je potrebno v ukazni vrstici pognati datoteko `run-extraction.py`, ki sprejme parameter `A`, `B` ali `C`.

	`A`: iz podanih 6 spletnih strani izlušči podatke z regex izrazi
	
	`B`: iz podanih 6 spletnih strani izlušči podatke z XPath
	
	`C`: na podlagi po 2 strani iz vsake domene generira ovojnice, iz katerih je možna nadaljnja ekstrakcija podatkov


## Index

### Opis
Pri tretjem delu projekta je bil izdelan invertni indeks za iskanje po podanih spletnih straneh. 
Za primerjavo, torej da bi ugotovili, koliko hitrejše je iskanje z uporabo invertnega indeksa kot iskanje brez njega, smo implementirali še navadno iskanje.



### Priprava in zagon
1. Potrebno je namestiti sledeče odvisnosti: `json`, `sqlite3`, `os`, `bs4`, `nltk`, `collections`, `sys`, `datetime` in `operator`.
2. Za pripravo SQLite podatkovne baze moramo pognati naslednji ukaz:

	```python
	sqlite3 implementation_indexing/baza.db < implementation_indexing/database.sql
	```
	
3. Za zagon iskanja z invertnim indeksom je potrebno v ukazni vrstici pognati ukaz `run-sqlite-search.py` in kot argument podati besede iz poizvedbe.
4. Za zagon iskanja brez invertnega indeksa je potrebno v ukazni vrstici pognati ukaz `run-basic-search.py` in kot argument podati besede iz poizvedbe.

