from os import listdir
from os.path import isfile, join
import process_data as pd
from collections import defaultdict
import sys
from datetime import datetime

def search_basic(query):
    websites = ['e-prostor.gov.si', 'e-uprava.gov.si', 'evem.gov.si', 'podatki.gov.si']
    # websites = ['e-prostor.gov.si']
    _, processed_query = pd.preprocess(query)
    # print(pre_query_filtered)

    query_freq = defaultdict(int)
    query_word_index = defaultdict(list)
    query_snippets = defaultdict(str)

    for website in websites:
        print(website)
        files = [f for f in listdir(f'data/{website}') if isfile(join(f'data/{website}', f))]

        for file in files:
            filename = f"{website}/{file}"

    # filename = websites[0] + "/e-prostor.gov.si.1.html"
            page = pd.read_file("data/" + filename)
            _, processed_page = pd.preprocess_page(page)
            for q in processed_query:
                indices = [i for i, x in enumerate(processed_page) if x == q]
                query_freq[filename] = query_freq[filename] + len(indices)
                query_word_index[filename] = query_word_index[filename] + indices
            query_snippets[filename] = get_snippets(processed_page, query_word_index[filename])
    # print(query_freq)

    results_num = 0
    for d in sorted(query_freq.items(), key=lambda x: x[1], reverse=True):
        if d[1]>0:
            results_num += 1
            print(d[0], d[1], query_snippets[d[0]])
    print(results_num, "results found")

def get_snippets(page, indices):
    snippet = ""
    neighborhood = 3
    ind = 0
    while ind<len(indices):
        # print(ind)
        ind_from = indices[ind]
        ind_to = indices[ind]

        while ind+1<len(indices):
            if indices[ind + 1] - ind_to <= neighborhood:
                # print("if")
                ind_to = indices[ind+1]
                ind += 1
            else:
                break
        # print(ind_from, ind_to)
        ind += 1
        if ind_from -neighborhood < 0:
            ind_from = 0
        else:
            ind_from = ind_from-neighborhood
        snippet += "..." + " ".join(page[ind_from: ind_to+neighborhood+1])


    snippet += "..."
    return snippet




if __name__ == "__main__":
    # for query in ["predelovalne dejavnosti", "trgovina", "social services"]:
    #     search_basic(query)

    query = sys.argv[1]
    print("Results for:", query)
    time = datetime.now()
    search_basic(query)
    td = datetime.now() - time
    print(td.seconds // 3600, "h", (td.seconds // 60) % 60, "min", td.seconds % 60, "s")

    # text = "Danes je lep dan. Tudi moj pes Frido je zelo lep. Tak dan, kot je danes, bi se lahko večkrat ponovil. Res lep dan."
    # _, text_proc = pd.preprocess(text)
    # query = "lep dan"
    # _, query_proc = pd.preprocess(query)
    # indices = []
    # for q in query_proc:
    #     print(q)
    #     indices += [i for i, x in enumerate(text_proc) if x == q]
    #     print(indices)
    # print(sorted(indices))
    # print(get_snippets(text_proc, sorted(indices)))

