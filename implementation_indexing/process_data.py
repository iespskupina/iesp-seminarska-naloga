import ast
import sqlite3
from os import listdir
from os.path import isfile, join
from bs4 import BeautifulSoup
import nltk
from nltk.corpus import stopwords
import json


conn = None


def connect_to_db():
    """ Return connection to database """
    global conn
    if conn is None:
        conn = sqlite3.connect('inverted-index.db', isolation_level=None)
    return conn


def read_file(path):

    with open(path, "r", encoding="utf-8") as f:
        page = BeautifulSoup(f, "lxml")
    return page


def preprocess_page(page):

    # remove style and script
    for i in page(['script', 'style']):
        i.extract()

    text = page.text

    return preprocess(text)


def preprocess(text):

    # tokenization
    tokens = nltk.word_tokenize(text, language="slovene")

    # normalization into lower case
    tokens = [token.lower() for token in tokens]

    # remove stopwords
    text_filtered = [word for word in tokens if word not in stopwords.words("slovene")]

    return text_filtered, tokens


def get_surroundings(index, text_with_stopwords):
    if index < 5:
        surrounding = text_with_stopwords[0:index + 5]
    elif index > len(text_with_stopwords) - 5:
        surrounding = text_with_stopwords[index - 5:len(text_with_stopwords)]
    else:
        surrounding = text_with_stopwords[index - 5:index + 5]

    return surrounding


def insert_into_db(page, website):

    text_filtered, text_with_stopwords = preprocess_page(page)

    # indexes and surroundings of words in text with stopwords
    d_stopwords = {}
    for index, word in enumerate(text_with_stopwords):
        if word not in d_stopwords.keys():
            surrounding = get_surroundings(index, text_with_stopwords)
            d_stopwords[word] = {"indexes": [str(index)], "surroundins": [surrounding]}
        else:
            surrounding = get_surroundings(index, text_with_stopwords)
            d_stopwords[word]["indexes"].append(str(index))
            d_stopwords[word]["surroundins"].append(surrounding)

    # count and indexes of words in text with no stopwords
    d = {}
    for index, word in enumerate(text_filtered):
        if word not in d.keys():
            d[word] = {"count": 1, "indexes": [str(index)]}
        else:
            d[word]["count"] += 1
            d[word]["indexes"].append(str(index))

    conn = connect_to_db()
    cur = conn.cursor()

    for key, value in d.items():
        count = value["count"]
        indexes = ",".join(value["indexes"])

        # indexes in text with stopwords
        # indexes = ",".join(d_stopwords[word]["indexes"])

        surr = d_stopwords[key]
        surr = json.dumps(surr)

        cur.execute("INSERT INTO IndexWord "
                    "SELECT ? "
                    "WHERE NOT EXISTS(SELECT 1 FROM IndexWord WHERE word = ?);",
                    (key, key))
        cur.execute("INSERT INTO Posting "
                    "VALUES (?, ?, ?, ?, ?)",
                    (key, website, count, indexes, surr))

    cur.close()


def main():
    # conn = connect_to_db()
    # cur = conn.cursor()
    # cur.execute("CREATE TABLE IndexWord (word TEXT PRIMARY KEY);")
    # cur.execute("CREATE TABLE Posting (word TEXT NOT NULL,documentName TEXT NOT NULL,frequency INTEGER NOT NULL,indexes TEXT NOT NULL,surrounding TEXT NOT NULL,PRIMARY KEY(word, documentName),FOREIGN KEY (word) REFERENCES IndexWord(word));")
    # cur.close()

    websites = ['e-prostor.gov.si', 'e-uprava.gov.si', 'evem.gov.si', 'podatki.gov.si']

    for website in websites:
        print(website)
        files = [f for f in listdir(f'data/{website}') if isfile(join(f'data/{website}', f))]

        for file in files:
            page = read_file(f"data/{website}/{file}")
            print(f"Working on {file}")
            insert_into_db(page, file)



if __name__ == "__main__":
    main()
