from datetime import datetime
import sys

import process_data as pd
from operator import itemgetter
import json

def find_document(documents, doc):
    for i in range(len(documents)):
        if documents[i][0] == doc:
            return i
    return -1


def search_index(query, limit=None):
    conn = pd.connect_to_db()
    cur = conn.cursor()
    _, filtriran_query = pd.preprocess(query)
    dokumenti = []
    for beseda in filtriran_query:
        if limit is None:
            cur.execute(
                "SELECT documentName, frequency, indexes, surrounding FROM Posting WHERE word = ? ORDER BY frequency DESC",
                (beseda,))
        else:
            cur.execute(
                "SELECT documentName, frequency, indexes, surrounding FROM Posting WHERE word = ? ORDER BY frequency DESC LIMIT ?",
                (beseda, limit))
        all_rows = cur.fetchall()
        for row in all_rows:
            okolica_json = json.loads(row[3])
            index_dokumenta = find_document(dokumenti, row[0])
            if index_dokumenta >= 0:
                dokumenti[index_dokumenta][1] += row[1]
                dokumenti[index_dokumenta][2]["indexes"] += okolica_json["indexes"]
                dokumenti[index_dokumenta][2]["surroundins"] += okolica_json["surroundins"]
            else:
                dokumenti.append([row[0], row[1], okolica_json])
    dokumenti = sorted(dokumenti, key=itemgetter(1), reverse=True)
    if limit is not None:
        dokumenti = dokumenti[:limit]

    izpisi_dokumente(dokumenti)
    pass


def get_website(dokument):
    return ".".join(dokument.split(".")[:3])


def besedila(indeksi):
    snippeti = [(x, y) for x, y in sorted(zip(indeksi["indexes"], indeksi["surroundins"]), key=lambda pair: pair[0])]
    besedilo = ""
    prvi = True
    prejsnji = -3
    for s in snippeti:
        if int(s[0]) - prejsnji <= 3:
            besedilo += " ".join(s[1][(6 - (int(s[0]) - prejsnji)):9])
        else:
            besedilo += " ... "
            besedilo += " ".join(s[1][2:9])
        prejsnji = int(s[0])
        prvi = False
    besedilo += " ... "
    return besedilo


def izpisi_dokumente(dokumenti):
    print(len(dokumenti), "results")
    print("Frequencies | Document                                           | Snippet")
    print("----------- | -------------------------------------------------- | -----------------------------------------------------------")
    for dokument in dokumenti:
        pot = "data" + "/" + get_website(dokument[0]) + "/" + dokument[0]
        print('{:11d} | {:<50s} | {:s}'.format(dokument[1], pot, besedila(dokument[2])))
        pass


if __name__ == '__main__':
    query = sys.argv[1]
    print("Results for:", query)
    time = datetime.now()
    search_index(query)
    td = datetime.now() - time
    print(td.microseconds/1000, "ms")
